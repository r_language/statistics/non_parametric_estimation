
# Non_parametric_estimation_survival_curve_1



In the R file provided there is a code that shows a non parametric estimation of the survival function. To do so, you will need the exo_cours.txt file that you can download. In R, you will have to use multiple libraries (survival, survminer, fitdistrplus, muhaz). In the code you will find :



- Graph to show the survival curve (survfit)



- Graph of cumulative hazard (survfit, ggsurvplot)



- Tests and distribution (log.rank.test, dweibull, dexp, dlnorm)



- Generate a time of events with Weibull distribution (rweibull, dweibull, pweibull)



- Estimation of the parameters of the weibull and exponential distribution (fitdist)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021






